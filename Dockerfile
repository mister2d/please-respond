FROM python:3-alpine

RUN set -x \
    && apk --no-cache --update add py3-pandas py3-requests

ADD ./aggregate.py /usr/local/bin

ENTRYPOINT /usr/local/bin/aggregate.py
