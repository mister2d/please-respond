#!/usr/bin/env python3.8

import json
import pandas as pd
import requests
from datetime import datetime


url = 'http://stream.meetup.com/2/rsvps'

# response = 'response'
# event = 'event.time'
# event_url = 'event.event_url'
# group_country = 'group.group_country'
# spec = { "future_date": "event.time", "future_url": "event.event_url", "country": "group.group_country" }

lines=[]
r = requests.get(url, stream=True)

for line in r.iter_lines(decode_unicode=True):
    if line:
        lines.append(json.loads(line))
        linesDF = pd.DataFrame(lines)
        eventsSeries = linesDF['event']
        eventsDF = pd.DataFrame(eventsSeries.to_list())
        
        groupsSeries = linesDF['group']
        groupsDF = pd.DataFrame(groupsSeries.to_list())

        topEventCountriesSeries = groupsDF.groupby('group_country')['group_country'].count().nlargest(3)
        topCountriesOutput = ''

        for i in topEventCountriesSeries.index:
            topCountriesOutput = topCountriesOutput + ',' + str(i) + ',' + str(topEventCountriesSeries[i])
        
        latestEventDF = eventsDF.groupby('time').max().tail(1)
        latestEventUnixTime = latestEventDF.index.asi8[0] / 1000
        latestEventTime = str(datetime.utcfromtimestamp(latestEventUnixTime).strftime('%Y-%m-%d %H:%M'))   
        latestEventUrl = str(latestEventDF.iloc[0]['event_url'])

        outputString = str(len(lines)) + ',' + latestEventTime + ',' + latestEventUrl + topCountriesOutput

        print(outputString)
